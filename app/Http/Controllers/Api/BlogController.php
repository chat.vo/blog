<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\StoreBlogRequest;
use App\Http\Requests\UpdateBlogRequest;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use App\Models\Blog;
use App\Models\Image;
class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $blogs = Blog::with('images')->get();
        return response()->json([
            'success' => true,
            'data' => $blogs
        ], 200);
    }

   
    public function store(StoreBlogRequest $request)
    {
        $slug = Str::slug($request->title);
        $baseSlug = $slug;
        $counter = 1;
    
        while (Blog::where('slug', $slug)->exists()) {
            $slug = $baseSlug . '(' . $counter .')';
            $counter++;
        }
    
        $blogData = [
            'title' => $request->title,
            'slug' => $slug,
            'description' => $request->description,
            'user_id' => $request->user_id
        ];
    
       
        DB::beginTransaction();
    
        $blog = Blog::create($blogData);
    
        if ($request->hasFile('images')) {
            foreach ($request->file('images') as $image) {
                $imageName = uniqid() . '_' . time() . '.' . $image->getClientOriginalExtension();
                $image->move(public_path() .'/blogs', $imageName);
                Image::create(['blog_id' => $blog->id, 'image' => $imageName]);
            }
        }
    
        if ($blog) {
            
            DB::commit();
    
            $blog['images'] = $blog->images;
    
            return response()->json([
                'success' => true,
                'data' => $blog
            ], 200);
        } else {
            
            DB::rollBack();
    
            return response()->json([
                'success' => false,
                'message' => 'Failed to store data'
            ], 500);
        }
    }
    

    /**
     * Display the specified resource.
     */
    public function show(int $id)
    {
        $blog = Blog::with('images')->find($id);
        
        if($blog){
            return response()->json([
                'success' => true,
                'data' => $blog
            ], 200);
        }
        return response()->json([
            'success' => false,
            'data' => null
        ], 404);
    }

    
    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateBlogRequest $request, int $id)
    {
            DB::beginTransaction();

            $slug = Str::slug($request->title);
            $baseSlug = $slug;
            $counter = 1;

            while (Blog::where('slug', $slug)->exists()) {
                $slug = $baseSlug . '(' . $counter .')';
                $counter++;
            }
            $blogData = [
                'title' => $request->title,
                'slug' => $slug,
                'description' => $request->description,
            ];

            Blog::where('id', $id)->update($blogData);
            
            if ($request->hasFile('images')) {

                $blog = Blog::find($id);

                foreach ($blog->images as $image) {
                    $filePath = public_path('blogs/' . $image->image);
                    if (file_exists($filePath)) {
                        unlink($filePath);
                    }
                    $image->delete();
                }

                foreach ($request->file('images') as $image) {
                    $imageName = uniqid() . '_' . time() . '.' . $image->getClientOriginalExtension();
                    $image->move(public_path() .'/blogs', $imageName);
                    Image::create(['blog_id' => $id, 'image' => $imageName]);
                }
            }
            if($blog){
            
                DB::commit();
    
                return response()->json([
                    'success' => true,
                    'message' => 'Updated successfully'
                ], 200);

            }else{
                DB::rollBack();
                return response()->json([
                    'success' => false,
                    'message' => 'Failed to update data.'
                ], 500);
            }
        
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(int $id)
    {
        $blog = Blog::find($id);
        
        if (!$blog) {
            return response()->json([
                'success' => false,
                'message' => 'Blog not found.'
            ], 404);
        }

       
        DB::beginTransaction();
        
        foreach ($blog->images as $image) {
            $filePath = public_path('blogs/' . $image->image);
            if (file_exists($filePath)) {
                unlink($filePath);
            }
            $image->delete();
        }

        $check = $blog->delete();
        
        if($check){
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => 'Deleted successfully'
            ], 200);
        }else{
            DB::rollBack();
            return response()->json([
                'success' => false,
                'message' => 'Failed to delete data.'
            ], 500);
        }     
    }
}